import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideMenuWithTabsComponent } from './slide-menu-with-tabs.component';

describe('SlideMenuWithTabsComponent', () => {
  let component: SlideMenuWithTabsComponent;
  let fixture: ComponentFixture<SlideMenuWithTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlideMenuWithTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideMenuWithTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

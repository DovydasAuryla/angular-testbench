import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-slide-menu-with-tabs',
  templateUrl: './slide-menu-with-tabs.component.html',
  styleUrls: ['./slide-menu-with-tabs.component.scss']
})
export class SlideMenuWithTabsComponent implements OnInit {

  @ViewChild('drawer', { static: false }) sideNav;

  constructor() { }

  ngOnInit() {
  }

  showSlideMenu() {
    this.sideNav.open();
  }
  
  closeSlideMenu(){
    this.sideNav.close();
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';

// MOCK OBJECTS
export interface Location {
  value: string;
  viewValue: string;
}
//

@Component({
  selector: 'app-plan-config',
  templateUrl: './plan-config.component.html',
  styleUrls: ['./plan-config.component.scss']
})
export class PlanConfigComponent implements OnInit {

  //--------MOCK DATA--------------//
  locations: Location[] = [
    { value: '0', viewValue: 'Item-1' },
    { value: '1', viewValue: 'Item-2' },
    { value: '2', viewValue: 'Item-3' },
    { value: '0', viewValue: 'Item-4' },
    { value: '1', viewValue: 'Item-6' },
    { value: '0', viewValue: 'Item-7' },
    { value: '1', viewValue: 'Item-8' }
  ];


  constructor(private http: HttpClient) {
    //-----------FOR LOCATION SELECT SEARCH------------------------//
    this.filteredLocations = this.locationCtrl.valueChanges
      .pipe(
        startWith(''),
        map(location => location ? this._filterLocations(location) : this.locations.slice())
      );
  }
  ngOnInit() {
  }

  //----------FOR LOCATION SELECT SEARCH------------------------//
  locationCtrl = new FormControl();
  filteredLocations: Observable<Location[]>;

  private _filterLocations(value: string): Location[] {
    const filterValue = value.toLowerCase();
    return this.locations.filter(location => location.viewValue.toLowerCase().indexOf(filterValue) === 0);
  }

  //--------------------- FOR FILE UPLOAD FIELD-----------------------//
  image = new Image();
  inputFileLabel: string = "Select plan image...";

  fileEvent(fileInput: Event) {
    let file = (<HTMLInputElement>fileInput.target).files[0];
    let fileName = file.name;
    const reader = new FileReader();
    reader.onload = () => {
      this.image.onload = () => {

        console.log("Plan width: " + this.image.naturalWidth + "px");
        console.log("Plan height: " + this.image.naturalHeight + "px");
        console.log(this.image);
      }
      this.image.src = reader.result as string;
    }
    reader.readAsDataURL(file);
    this.inputFileLabel = fileName;
  }

  //--------------------- AFTER PLAN GENERAL INFO SUBMIT-----------------------//

  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;
  selectedTab = new FormControl(0);  // 0 = GENERAL TAB | 1 = CONFIGURATION

  afterGeneralInfoSubmit() {
    this.selectedTab = new FormControl(1);
  }

  //Create CONFIGURATION tab content and wrap uploaded picture with canvas
  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.canvas = <HTMLCanvasElement>document.getElementById("plan-configuration-tab-canvas");
    this.ctx = this.canvas.getContext("2d");    
    this.canvas.width = this.image.naturalWidth;
    this.canvas.height = this.image.naturalHeight;
    this.ctx.drawImage(this.image, 0, 0);
    // console.log('tabChangeEvent => ', tabChangeEvent);
    // console.log('index => ', tabChangeEvent.index);
  }

  //TABLE
  displayedColumns = ['position', 'name'];
  dataSource = ELEMENT_DATA;
}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];
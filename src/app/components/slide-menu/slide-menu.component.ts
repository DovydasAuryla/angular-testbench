import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-slide-menu',
  templateUrl: './slide-menu.component.html',
  styleUrls: ['./slide-menu.component.scss']
})
export class SlideMenuComponent implements OnInit {

  @ViewChild('drawer', { static: false }) sideNav;

  constructor() { }

  ngOnInit() {
  }

  showSlideMenu() {
    this.sideNav.open();
  }

}

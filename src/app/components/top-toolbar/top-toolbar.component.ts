import { Component, OnInit } from '@angular/core';
import { SlideMenuWithTabsComponent } from '../slide-menu-with-tabs/slide-menu-with-tabs.component';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-top-toolbar',
  templateUrl: './top-toolbar.component.html',
  styleUrls: ['./top-toolbar.component.scss']
})
export class TopToolbarComponent implements OnInit {


  color = 'primary';
  mode = 'determinate';
  value = 50;

  constructor(private slideMenuWithTabsComponent: SlideMenuWithTabsComponent) { }

  ngOnInit() {
  }

  showSlideMenu() {
    this.slideMenuWithTabsComponent.showSlideMenu();
  }

}
